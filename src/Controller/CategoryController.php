<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;

class CategoryController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/category/{id}", name="category")
     */
    public function index($id)
    {
        $category = $this->categoryRepository->find($id);
        $productCount = $this->productRepository->count([
            'category' => $category
        ]);
        $availableProductCount = $this->productRepository->count([
            'category' => $category,
            'isAvailable' => true
        ]);
        return $this->render('category/index.html.twig', [
            'productCount' => $productCount,
            'availableProductCount' => $availableProductCount
        ]);
    }
}
