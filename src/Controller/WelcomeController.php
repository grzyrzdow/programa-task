<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WelcomeController extends AbstractController
{
    /**
     * @Route("/", name="welcome")
     */
    public function welcome(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product)->add('submit', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
            return $this->redirectToRoute("new_product", [
                'id' => $product->getId()
            ]);
        }

        return $this->render('welcome/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/product/{id}", name="new_product")
     */
    public function productView($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        return $this->render("product/view.html.twig", [
            'product' => $product
        ]);
    }
}
